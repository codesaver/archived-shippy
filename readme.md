# Shippy

Example microservices architecture implementation in Golang use `go-micro`

## Build & Run
In order to build and run this example, run following commands:

- `make build service=greeter`
- `docker-compose up`
 
 Then go to `http://localhost:8082/client` on your browser
 - choose service `shippy.service.greeter`
 - Choose endpoint `Greeter.Hello`
 - Send

## Ref

- [https://micro.mu/docs/go-micro.html](https://micro.mu/docs/go-micro.html)
